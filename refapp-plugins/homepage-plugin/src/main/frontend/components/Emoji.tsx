import React, { FC } from 'react';

interface EmojiProps {
    // A textual description of the concept being conveyed by the emoji
    label?: string;
    // The literal emoji you want to render.
    symbol: string;
}

const Emoji: FC<EmojiProps> = ({ label, symbol }) => (
    <span
        className="emoji"
        role="img"
        aria-label={label || ''}
        aria-hidden={label ? 'false' : 'true'}
    >
        {symbol}
    </span>
);

export default Emoji;
