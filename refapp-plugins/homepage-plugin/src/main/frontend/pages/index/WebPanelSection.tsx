import React, { FC } from 'react';
import { PanelHandler, useExtensionsAll } from '@atlassian/clientside-extensions-components';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';

const xmlDescriptorExample = `
<web-panel key="myPanel" location="atl.refapp.index">
    <resource name="view" type="velocity"
    location="/relative/or/absolute/path/to/my/panel.vm"/>
</web-panel>
`;

function asDynamicPanels(ext: PluginDescriptor) {
    const { label, onAction } = ext.attributes;
    return (
        <React.Fragment key={ext.key}>
            <h3>{label}</h3>
            <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />
        </React.Fragment>
    );
}

function asStaticPanels(ext: PluginDescriptor) {
    return 'To be implemented';
}

const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['panel'],
        },
        label: {
            type: 'string',
            description: 'An optional header for the panel content',
        },
        onAction: {
            typeof: 'function',
            description: 'Used to register the lifecycle of the panel',
        },
    },
};

const loadingIndicator = (
    <p>
        Loading <code>index.links</code>...
    </p>
);

const WebPanelSection: FC = () => {
    const [newerExtensions, olderExtensions, loading] = useExtensionsAll('atl.refapp.index', null, {
        schema,
    });
    return (
        <>
            <h2>Web Panels</h2>

            <div id="js-index-webpanels">
                {loading && loadingIndicator}
                {!loading && newerExtensions.map(asDynamicPanels)}
                {!loading && olderExtensions.map(asStaticPanels)}
            </div>

            <details>
                <summary>What about the content above?</summary>

                <p>
                    In addition to Web Items, you can also add <dfn>Web Panels</dfn> here.
                </p>
                <ul>
                    <li>
                        Using Client-Side Extensions.{' '}
                        <a href="https://developer.atlassian.com/server/framework/clientside-extensions/reference/api/panel-api/">
                            Read the docs for Client-Side Extensions: <code>PanelExtension</code>{' '}
                            API
                        </a>
                    </li>
                    <li>
                        Using XML descriptors.
                        <code>
                            <pre>{xmlDescriptorExample}</pre>
                        </code>
                    </li>
                </ul>
            </details>
        </>
    );
};

export default WebPanelSection;
