import React, { FC } from 'react';
import { I18n } from '@atlassian/wrm-react-i18n';
import RefappPageContent from '../RefappPageContent';
import WebItemSection from './WebItemSection';
import WebPanelSection from './WebPanelSection';
import Emoji from '../../components/Emoji';

const IndexPage: FC = () => {
    return (
        <RefappPageContent title={I18n.getText('refapp.homepage.index.title')}>
            <div className="aui-message info">
                <p>
                    You might want to log in <Emoji symbol="😉" label="wink" />
                </p>
                <p>
                    By default, three users are available, admin, fred and barney. Their passwords
                    are the same as their usernames.
                </p>
            </div>

            <p>
                This application has no useful features whatsoever. It is intended to be used as a
                platform for testing cross-product Atlassian plugins.
            </p>

            <WebItemSection />
            <WebPanelSection />
        </RefappPageContent>
    );
};

export default IndexPage;
