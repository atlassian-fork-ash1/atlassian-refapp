package com.atlassian.refapp.pages;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class HomepageServlet implements Filter {
    private final PageBuilderService pageBuilderService;
    private final TemplateRenderer templateRenderer;

    @Autowired
    public HomepageServlet(
        @ComponentImport PageBuilderService pageBuilderService,
        @ComponentImport TemplateRenderer templateRenderer) {
        this.pageBuilderService = pageBuilderService;
        this.templateRenderer = templateRenderer;
    }

    @Override
    public void init(final FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        boolean handled = false;
        if (request instanceof HttpServletRequest) {
            handled = maybeRenderPageContents((HttpServletRequest) request, (HttpServletResponse) response);
        }
        if (!handled) {
            chain.doFilter(request, response);
        }
    }

    private boolean maybeRenderPageContents(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final String servletPath = request.getServletPath();
        final String requestURI = request.getRequestURI();
        if (servletPath.equals("/index.jsp") && !requestURI.contains("index.jsp")) {
            // The container has sent us to the welcome page
            pageBuilderService.assembler().resources().requireContext("homepage.resources.index");
            render("refapp-homepage-ui", "atl.general", response);
            return true;
        }
        if (servletPath.equals("/admin") || servletPath.equals("/admin/")) {
            pageBuilderService.assembler().resources().requireContext("homepage.resources.admin");
            render("refapp-adminpage-ui", "atl.admin", response);
            return true;
        }
        return false;
    }

    private void render(String template, String decoratorName, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        // I would typically server-side render the React components, but the platform doesn't have an ability to do that.
        //
        // A reasonable fallback would be for me to render this via some other templating language... but I'm also
        // not doing that, mainly due to laziness.
        response.getWriter().write("<!DOCTYPE html><html><head><meta name=\"decorator\" content=\"" + decoratorName + "\"/></head><body><div id=\"" + template + "\">Loading...</div></body>");
        response.getWriter().flush();
    }
}
