const TerserPlugin = require('terser-webpack-plugin');
const merge = require('webpack-merge');
const webpack = require('webpack');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');

const {
    PLUGIN_KEY,
    WRM_OUTPUT,
    DEV_SERVER_HOST,
    DEV_SERVER_PORT,
    FRONTEND_OUTPUT_DIR,
    FRONTEND_SRC_DIR,
} = require('./webpack.constants');

const { loaders } = require('./webpack.loaders');

const watchConfig = {
    devServer: {
        host: DEV_SERVER_HOST,
        port: DEV_SERVER_PORT,
        overlay: true,
        hot: true,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': '*',
        },
    },
    plugins: [new webpack.HotModuleReplacementPlugin()],
    devtool: 'inline-source-map',
};

const devConfig = env => {
    return merge([
        {
            optimization: {
                minimize: false,
                runtimeChunk: false,
                splitChunks: false,
            },
            output: {
                publicPath: `http://${DEV_SERVER_HOST}:${DEV_SERVER_PORT}/`,
                filename: '[name].js',
                chunkFilename: '[name].chunk.js',
            },
        },
        env === 'dev-server' && watchConfig,
    ]);
};

const prodConfig = {
    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    mangle: {
                        reserved: ['I18n', 'getText'],
                    },
                },
            }),
        ],
    },
};

module.exports = (env, argv = {}) => {
    const isProduction = argv.mode !== 'development';
    const mode = isProduction ? 'production' : 'development';
    const modeConfig = isProduction ? prodConfig : devConfig(env);
    return merge([
        {
            mode,
            entry: {
                index: './index.tsx',
                admin: './admin.tsx',
            },
            resolve: {
                extensions: ['*', '.ts', '.tsx', '.js', '.jsx'],
            },
            stats: {
                logging: 'info',
            },
            context: FRONTEND_SRC_DIR,
            plugins: [
                new WrmPlugin({
                    pluginKey: PLUGIN_KEY,
                    xmlDescriptors: WRM_OUTPUT,
                    singleRuntimeWebResourceKey: 'rate-limiting-plugin-runtime',
                    contextMap: {
                        index: 'homepage.resources.index',
                        admin: 'homepage.resources.admin',
                    },
                    watch: !isProduction,
                    watchPrepare: !isProduction,
                }),
                new DuplicatePackageCheckerPlugin(),
            ],
            module: {
                rules: loaders(isProduction),
            },
            output: {
                path: FRONTEND_OUTPUT_DIR,
            },
        },
        modeConfig,
    ]);
};
