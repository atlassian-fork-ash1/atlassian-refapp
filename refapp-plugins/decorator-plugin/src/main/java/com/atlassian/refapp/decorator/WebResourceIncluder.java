package com.atlassian.refapp.decorator;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import java.io.CharArrayWriter;

public class WebResourceIncluder {
    private final PageBuilderService pageBuilderService;

    public WebResourceIncluder(PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }

    @com.atlassian.velocity.htmlsafe.HtmlSafe
    public CharArrayWriter includeResources() {
        CharArrayWriter writer = new CharArrayWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.RELATIVE);
        return writer;
    }
}
