package com.atlassian.refapp.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * A filter that looks for a query param that sets the Product Definition Language variable
 */
public class PdlDarkToggleFilter implements Filter {
    private final Logger log = LoggerFactory.getLogger(getClass());

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String pdl = servletRequest.getParameter("pdl.dir");
        if (pdl != null) {
            System.setProperty("pdl.dir", pdl);
            log.info("Product Design Language variable ${pdl.dir} now set to: {}", pdl);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {
    }


}
