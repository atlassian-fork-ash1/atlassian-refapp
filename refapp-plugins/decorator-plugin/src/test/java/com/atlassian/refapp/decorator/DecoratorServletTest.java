package com.atlassian.refapp.decorator;

import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.module.sitemesh.RequestConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DecoratorServletTest {
    @Mock
    TemplateRenderer templateRenderer;
    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    ServletConfig servletConfig;
    @Mock
    ServletContext servletContext;
    @Mock
    SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    Page page;

    @Mock
    WebSudoSessionManager webSudoSessionManager;

    @Before
    public final void setUpMockBehaviour() {
        when(request.getAttribute(RequestConstants.PAGE)).thenReturn(page);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
    }

    @Test
    public void noExceptionsAreThrownWhenPropertiesAreNotAvailable() throws ServletException, IOException {
        DecoratorServlet ds = new DecoratorServlet(templateRenderer, webSudoSessionManager, soyTemplateRenderer);
        ds.init(servletConfig);
        ds.service(request, response);
    }

    @Test
    public void exceptionsAreNotPropagatedWhenStreamReadFails() throws ServletException, IOException {
        InputStream immediatelyFailingInputStream = mock(InputStream.class);
        when(immediatelyFailingInputStream.read(any())).thenThrow(new IOException());

        when(servletContext.getResourceAsStream(any())).thenReturn(immediatelyFailingInputStream);
        DecoratorServlet ds = new DecoratorServlet(templateRenderer, webSudoSessionManager, soyTemplateRenderer);
        ds.init(servletConfig);
        ds.service(request, response);
    }
}
