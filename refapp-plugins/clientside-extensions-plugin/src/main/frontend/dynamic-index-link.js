import contextPath from 'wrm/context-path';

/**
 * @clientside-extension
 * @extension-point index.links
 */
export default () => ({
    type: 'link',
    label: 'A dynamically-created link to UPM',
    url: contextPath() + '/plugins/servlet/upm/manage/all'
});
