package com.atlassian.streams.refapp;

import com.atlassian.streams.spi.StreamsValidator;

public class RefappStreamsValidator implements StreamsValidator {
    @Override
    public boolean isValidKey(String key) {
        return RefappKeyProvider.REFAPP_KEY.equals(key);
    }
}
