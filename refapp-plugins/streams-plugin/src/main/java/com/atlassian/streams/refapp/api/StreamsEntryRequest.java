package com.atlassian.streams.refapp.api;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import org.joda.time.DateTime;

public class StreamsEntryRequest {
    private int id;
    private DateTime postedDate = new DateTime();
    private String title;
    private String user;
    private ActivityObjectType type;
    private ActivityVerb verb;

    public int getId() {
        return id;
    }

    public StreamsEntryRequest id(final int id) {
        this.id = id;
        return this;
    }

    public DateTime getPostedDate() {
        return postedDate;
    }

    public StreamsEntryRequest postedDate(final DateTime postedDate) {
        this.postedDate = postedDate;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public StreamsEntryRequest title(final String title) {
        this.title = title;
        return this;
    }

    public String getUser() {
        return user;
    }

    public StreamsEntryRequest user(final String user) {
        this.user = user;
        return this;
    }

    public ActivityVerb getVerb() {
        return verb;
    }

    public StreamsEntryRequest verb(final ActivityVerb verb) {
        this.verb = verb;
        return this;
    }

    public ActivityObjectType getType() {
        return type;
    }

    public StreamsEntryRequest type(final ActivityObjectType type) {
        this.type = type;
        return this;
    }
}
