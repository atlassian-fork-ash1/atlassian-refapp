package com.atlassian.refapp.sal.pluginsettings;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.refapp.sal.FileBackedSettingsFileFactory;
import com.atlassian.refapp.sal.FileBackedSettingsService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * This implementation can be backed by a file on the file system.  If a file in the current working directory called
 * "com.atlassian.refapp.sal.pluginsettings.xml" exists (can be overridden with system property sal.com.atlassian.refapp.sal.pluginsettings.store) exists, it loads and
 * persists all plugin settings to and from this file.  If no file exists, plugin settings are purely in memory.
 */
@ExportAsService
@Named("pluginSettingsFactory")
public class RefimplPluginSettingsFactory extends FileBackedSettingsService implements PluginSettingsFactory {
    private static final String CHARLIE_KEYS = "charlie.keys";

    private static final Logger log = Logger.getLogger(RefimplPluginSettingsFactory.class);

    @Inject
    public RefimplPluginSettingsFactory(ApplicationProperties applicationProperties) {
        super(FileBackedSettingsFileFactory.getFileAndProperties("sal.com.atlassian.refapp.sal.pluginsettings.store", "com.atlassian.refapp.sal.pluginsettings.xml", Boolean.valueOf(System.getProperty("sal.com.atlassian.refapp.sal.pluginsettings.usememorystore", "false")), applicationProperties));
    }

    public PluginSettings createSettingsForKey(String key) {
        if (key != null) {
            List<String> charlies = (List<String>) new RefimplPluginSettings(new SettingsMap(null)).get(CHARLIE_KEYS);
            if (charlies == null || !charlies.contains(key)) {
                throw new IllegalArgumentException("No Charlie with key " + key + " exists.");
            }
        }
        return new RefimplPluginSettings(new SettingsMap(key));
    }

    public PluginSettings createGlobalSettings() {
        return createSettingsForKey(null);
    }

}
