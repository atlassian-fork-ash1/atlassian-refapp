package com.atlassian.webdriver.refapp.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.refapp.component.RefappHeader;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

public abstract class RefappAbstractPage implements Page {
    @Inject
    protected WebDriver driver;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    @ElementBy(className = "refapp-footer")
    private PageElement refappFooter;

    public RefappHeader getHeader() {
        return pageBinder.bind(RefappHeader.class);
    }
}
