package com.atlassian.webdriver.refapp.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.webdriver.utils.Check;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * Refapp WebSudo page
 */
public class RefappWebSudoPage extends RefappAbstractPage implements WebSudoPage {
    @Inject
    private PageBinder pageBinder;

    @ElementBy(name = "os_password")
    private PageElement passwordField;
    @ElementBy(id="websudo")
    private PageElement websudoButton;

    public <T extends Page> T confirm(Class<T> targetPage) {
        return confirm(null, targetPage);
    }

    public <T extends Page> T confirm(String password, Class<T> targetPage) {
        passwordField.type(password);
        websudoButton.click();

        return pageBinder.navigateToAndBind(targetPage);
    }

    public String getUrl() {
        return "/plugins/servlet/websudo";
    }

    public boolean isRequestAccessMessagePresent() {
        return Check.elementExists(By.id("request-access-message"), driver);
    }
}
