package com.atlassian.plugin.refimpl.saldeps;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.message.MessageCollection;
import io.atlassian.util.concurrent.ManagedLock;
import io.atlassian.util.concurrent.ManagedLocks;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * This is a copy of com.atlassian.refapp.sal.message.RefimplI18nResolver. It's been copied because it's required
 * by {@link com.atlassian.plugin.refimpl.ContainerManager} *before* the plugin system is available.
 *
 * This is not a direct clone of RefimplI18nResolver; it copies in both
 * com.atlassian.sal.core.message.AbstractI18nResolver and RefimplI18nResolver as sal-core is not available either.
 */
public class CoreRefimplI18nResolver implements I18nResolver {
    private ManagedLock.ReadWrite locks = ManagedLocks.manageReadWrite(new ReentrantReadWriteLock());
    private final Map<Plugin, Iterable<String>> pluginResourceBundleNames = new ConcurrentHashMap<>();

    public CoreRefimplI18nResolver(final PluginAccessor pluginAccessor,
                                   PluginEventManager pluginEventManager) {
        pluginEventManager.register(this);

        locks.write().withLock(() -> addPluginResourceBundles(pluginAccessor.getPlugins()));
    }

    private static final Serializable[] EMPTY_SERIALIZABLE = new Serializable[0];

    public String getText(String key, Serializable... arguments) {
        Serializable[] resolvedArguments = new Serializable[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            Serializable argument = arguments[i];
            if (argument instanceof Message) {
                resolvedArguments[i] = getText((Message) argument);
            } else {
                resolvedArguments[i] = arguments[i];
            }
        }
        return resolveText(key, resolvedArguments);
    }

    public String getText(Locale locale, String key, Serializable... arguments) {
        requireNonNull(locale, "locale");
        Serializable[] resolvedArguments = new Serializable[arguments.length];
        for (int i = 0; i < arguments.length; i++) {
            Serializable argument = arguments[i];
            if (argument instanceof Message) {
                resolvedArguments[i] = getText(locale, (Message) argument);
            } else {
                resolvedArguments[i] = arguments[i];
            }
        }
        return resolveText(locale, key, resolvedArguments);
    }

    public String getText(String key) {
        return resolveText(key, EMPTY_SERIALIZABLE);
    }

    public String getText(Locale locale, String key) {
        requireNonNull(locale, "locale");
        return resolveText(locale, key, EMPTY_SERIALIZABLE);
    }

    public String getText(Message message) {
        return getText(message.getKey(), message.getArguments());
    }

    public String getText(Locale locale, Message message) {
        return getText(locale, message.getKey(), message.getArguments());
    }

    public Message createMessage(String key, Serializable... arguments) {
        return new DefaultMessage(key, arguments);
    }

    public MessageCollection createMessageCollection() {
        return new DefaultMessageCollection();
    }

    @Override
    public String getRawText(String key) {
        return StringUtils.defaultString(getPattern(Locale.getDefault(), key), key);
    }

    @Override
    public String getRawText(Locale locale, String key) {
        return StringUtils.defaultString(getPattern(locale, key), key);
    }

    public String resolveText(String key, Serializable[] arguments) {
        String pattern = getPattern(Locale.getDefault(), key);
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    public String resolveText(final Locale locale, final String key, final Serializable[] arguments) {
        String pattern = StringUtils.defaultString(getPattern(locale, key), getPattern(Locale.getDefault(), key));
        if (pattern == null) {
            return key;
        }
        return MessageFormat.format(pattern, (Object[]) arguments);
    }

    private String getPattern(final Locale locale, final String key) {
        return locks.read().withLock((Supplier<String>) () -> {
            String bundleString = null;
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                for (String bundleName : pluginBundleNames.getValue()) {
                    try {
                        ResourceBundle bundle = getBundle(bundleName, locale, pluginBundleNames.getKey());
                        bundleString = bundle.getString(key);
                    } catch (MissingResourceException e) {
                        // ignore, try next bundle
                    }
                }
            }
            return bundleString;
        });
    }

    public Map<String, String> getAllTranslationsForPrefix(final String prefix) {
        requireNonNull(prefix, "prefix");

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {

                addMatchingTranslationsToMap(
                        prefix, Locale.getDefault(), pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }

    public Map<String, String> getAllTranslationsForPrefix(final String prefix, final Locale locale) {
        requireNonNull(prefix, "prefix");
        requireNonNull(locale, "locale");

        return locks.read().withLock((Supplier<Map<String, String>>) () -> {
            Map<String, String> translationsWithPrefix = new HashMap<>();
            for (Entry<Plugin, Iterable<String>> pluginBundleNames : pluginResourceBundleNames.entrySet()) {
                addMatchingTranslationsToMap(
                        prefix, locale, pluginBundleNames.getKey(), pluginBundleNames.getValue(), translationsWithPrefix);
            }
            return translationsWithPrefix;
        });
    }

    private void addMatchingTranslationsToMap(String prefix, Locale locale, Plugin plugin,
                                              Iterable<String> bundleNames,
                                              Map<String, String> translationsWithPrefix) {
        for (String bundleName : bundleNames) {
            try {
                ResourceBundle bundle = getBundle(bundleName, locale, plugin);
                if (bundle != null) {
                    addMatchingTranslationsToMap(prefix, bundle, translationsWithPrefix);
                }
            } catch (MissingResourceException e) {
                // OK, just ignore
            }
        }
    }

    private void addMatchingTranslationsToMap(String prefix, ResourceBundle bundle,
                                              Map<String, String> translationsWithPrefix) {
        Enumeration enumeration = bundle.getKeys();
        while (enumeration.hasMoreElements()) {
            String key = (String) enumeration.nextElement();
            if (key.startsWith(prefix)) {
                translationsWithPrefix.put(key, bundle.getString(key));
            }
        }
    }

    @PluginEventListener
    public void onPluginEnabled(final PluginEnabledEvent event) {
        locks.write().withLock(() -> addPluginResourceBundles(event.getPlugin()));
    }

    @PluginEventListener
    public void onPluginDisabled(final PluginDisabledEvent event) {
        locks.write().withLock(() -> removePluginResourceBundles(event.getPlugin()));
    }

    private void addPluginResourceBundles(Iterable<Plugin> plugins) {
        for (Plugin plugin : plugins) {
            addPluginResourceBundles(plugin);
        }
    }

    private void addPluginResourceBundles(Plugin plugin) {
        List<String> bundleNames = plugin.getResourceDescriptors().stream()
                .filter(descriptor -> "i18n".equals(descriptor.getType()))
                .map(ResourceDescriptor::getLocation)
                .collect(Collectors.toList());
        addPluginResourceBundles(plugin, bundleNames);
    }

    private void addPluginResourceBundles(Plugin plugin, List<String> bundleNames) {
        pluginResourceBundleNames.put(plugin, bundleNames);
    }

    private void removePluginResourceBundles(Plugin plugin) {
        pluginResourceBundleNames.remove(plugin);
    }

    private ResourceBundle getBundle(String bundleName, Locale locale, Plugin plugin) {
        return ResourceBundle.getBundle(bundleName, locale, plugin.getClassLoader());
    }
}
