package com.atlassian.plugin.refimpl.tenant;

import com.atlassian.plugin.refimpl.ParameterUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugins.landlord.spi.LandlordRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;

/**
 * If tenancy is not enabled - immediately trigger phase 2
 *
 * @since v2.21.4
 */
public class RefappTenancyManager {
    private static final Logger log = LoggerFactory.getLogger(RefappTenancyManager.class);
    private final RefappLandlordRequests landlordRequests;

    public RefappTenancyManager(final RefappLandlordRequests landlordRequests) {
        this.landlordRequests = landlordRequests;
    }

    public void enableTenancy() {
        if (!RefappTenancyCondition.isEnabled()) {
            final String tenantId = getBaseUrlWithoutProtocol(ParameterUtils.getBaseUrl(UrlMode.ABSOLUTE));
            try {
                landlordRequests.acceptTenant(tenantId);
            } catch (final LandlordRequestException elr) {
                log.error("Unable to accept tenant", elr);
            }
        }
    }


    private String getBaseUrlWithoutProtocol(final String baseUrl) {
        try {
            final URL url = new URL(baseUrl);
            return url.getAuthority() + url.getPath();
        } catch (final Exception e) {
            log.warn("Could not remove url protocol from '{}' : {}", baseUrl, e.getMessage());
            return baseUrl;
        }
    }
}
