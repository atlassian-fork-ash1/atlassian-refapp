package com.atlassian.plugin.refimpl.saldeps;

import com.atlassian.sal.api.message.Message;

import java.io.Serializable;

/**
 * This is a direct copy of com.atlassian.sal.core.message.DefaultMessage as sal-core is not available to the
 * ContainerManager.
 */
class DefaultMessage implements Message {
    private final Serializable[] arguments;
    private String key;

    public DefaultMessage(String key, Serializable... arguments) {
        this.key = key;
        this.arguments = arguments;
    }

    public Serializable[] getArguments() {
        return arguments;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(key);
        builder.append(": ");
        for (Serializable argument : arguments) {
            builder.append(argument);
            builder.append(",");
        }
        return builder.toString();
    }
}

