package com.atlassian.plugin.refimpl;

import com.atlassian.plugin.manager.PluginEnabledState;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.plugin.manager.PluginEnabledState.getPluginEnabledStateWithCurrentTime;

public class DefaultPluginPersistentStateStore implements PluginPersistentStateStore {
    private static final Logger log = LoggerFactory.getLogger(DefaultPluginPersistentStateStore.class);

    private File file;

    public DefaultPluginPersistentStateStore(final File directory) {
        try {
            file = new File(directory.getParentFile(), "plugins.state");
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (final IOException e) {
            log.error("Error creating plugins.state file. " + e, e);
        }
    }

    public PluginPersistentState load() {
        PluginPersistentState.Builder builder = PluginPersistentState.Builder.create();
        try (FileInputStream inputStream = new FileInputStream(file)) {
            final Properties properties = new Properties();
            properties.load(inputStream);
            builder.addPluginEnabledState(properties.entrySet().stream()
                    .collect(Collectors.toMap(
                            entry -> String.valueOf(entry.getKey()),
                            entry -> getPluginEnabledStateWithCurrentTime(Boolean.parseBoolean(String.valueOf(entry.getValue()))))));
        } catch (final IOException e) {
            log.error("Error creating/reading plugins.state file. ", e);
        }
        return builder.toState();
    }

    public void save(final PluginPersistentState state) {
        final Properties properties = new Properties();
        final Set<Entry<String, PluginEnabledState>> entrySet = state.getStatesMap().entrySet();
        for (final Entry<String, PluginEnabledState> entry : entrySet) {
            properties.put(entry.getKey(), String.valueOf(entry.getValue().isEnabled()));
        }
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            properties.store(outputStream, "Saving plugins state");
        } catch (final IOException e) {
            log.error("Error saving to plugins.state file. " + e, e);
        }
    }
}
