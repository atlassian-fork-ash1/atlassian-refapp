package com.atlassian.plugin.refimpl;

import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Initializes app
 */
public class InitListener implements ServletContextListener {

    public InitListener() {
    }

    public void contextInitialized(ServletContextEvent sce) {

        LoggerFactory.getLogger(InitListener.class).info("Logging initialized.");
        ContainerManager.setInstance(new ContainerManager(sce.getServletContext()));
        ContainerManager mgr = ContainerManager.getInstance();
        mgr.getPluginAccessor().getPlugins();
        if (System.getProperty("baseurl.display") != null) {
            LoggerFactory.getLogger(InitListener.class).info("\n\n*** Refapp started on " + System.getProperty("baseurl.display") + "\n\n");
        }
    }

    public void contextDestroyed(ServletContextEvent sce) {
        ContainerManager mgr = ContainerManager.getInstance();
        if (mgr != null) {
            mgr.shutdown();
        }
        ContainerManager.setInstance(null);
    }
}
