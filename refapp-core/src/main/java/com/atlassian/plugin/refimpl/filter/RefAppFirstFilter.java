package com.atlassian.plugin.refimpl.filter;

import com.atlassian.plugin.refimpl.CurrentHttpRequest;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter that takes the raw http request and stores it in {@link CurrentHttpRequest} so that it is accessible to other
 * classes. This is the first filter that runs in the filter chain.
 */

public class RefAppFirstFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Captures the current {@link ServletRequest} using the {@link CurrentHttpRequest} class
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        CurrentHttpRequest.setRequest((HttpServletRequest) servletRequest);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
