# Whitelist for plugin and module healthchecks, read by com.atlassian.healthcheck.spi.impl.ClasspathFileHealthCheckWhitelist
# See also TestPluginStartup / TestHealthChecks

# Temporary whitelisting of things that were failing when the test was introduced;
# DO NOT ADD THINGS TO THIS LIST!!
# A failure here means there's a broken plugin or module. Instead, fix the cause.

# We don't care about these. See CANL-19
DisabledPluginModulesHealthCheck com.atlassian.plugins.atlassian-nav-links-plugin:atlassian-nav-links-whitelist
DisabledPluginModulesHealthCheck com.atlassian.upm.atlassian-universal-plugin-manager-plugin:analyticsWhitelist
DisabledPluginModulesHealthCheck com.atlassian.applinks.applinks-plugin:applinks-whitelist

# TODO: PLUGWEB-361
# These two are broken because these '<web-modules>' modules are instantiated in webresource-plugin/webresource-rest,
# but the com.atlassian.plugin.webresource.impl.modules.ModulesDescriptor that implements <web-modules>
# has to be registered directly in the product rather than by the webresource plugin,
# and it isn't yet registered.
DisabledPluginModulesHealthCheck com.atlassian.plugins.atlassian-plugins-webresource-plugin:modules-not-used
DisabledPluginModulesHealthCheck com.atlassian.plugins.atlassian-plugins-webresource-rest:modules-not-used
