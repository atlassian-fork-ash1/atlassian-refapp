package it.com.atlassian.refapp;

public final class RefappTestURLs {

    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    public static final String BASEURL = "http://localhost:" + PORT + CONTEXT + "/";

    private RefappTestURLs() {}
}
