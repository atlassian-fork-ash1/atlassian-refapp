package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import com.atlassian.webdriver.refapp.page.RefappUsersPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestLogin extends AbstractRefappTestCase {
    private RefappLoginPage loginPage;

    @Before
    public final void setUp() throws Exception {
        loginPage = PRODUCT.gotoLoginPage();
    }

    @After
    public final void tearDown() throws Exception {
        loginPage.getHeader().logout(RefappHomePage.class);
    }

    @Test
    public void testLogout() {
        loginPage = loginPage.login("barney", "barney", RefappLoginPage.class);
        RefappHomePage homePage = loginPage.getHeader().logout(RefappHomePage.class);

        assertFalse("Must have logged out.", homePage.getHeader().isLoggedIn());
    }

    @Test
    public void testBadAdminLogin() {
        loginPage = loginPage.login("blahuser", "blahpassword", RefappLoginPage.class);
        assertFalse("The login must have failed.", loginPage.getHeader().isLoggedIn());
    }

    @Test
    public void testGoodAdminLogin() {
        RefappHomePage homePage = loginPage.login("betty", "betty", RefappHomePage.class);
        assertTrue("The login must have been successful.", homePage.getHeader().isLoggedIn());
        assertTrue("Must have logged in as admin.", homePage.getHeader().isAdmin());
    }

    @Test
    public void testGoodSysAdminLogin() {
        RefappHomePage homePage = loginPage.login("admin", "admin", RefappHomePage.class);
        assertTrue("The login must have been successful.", homePage.getHeader().isLoggedIn());
        assertTrue("Must have logged in as sysadmin.", homePage.getHeader().isSysadmin());
    }

    @Test
    public void testUserLogin() {
        RefappHomePage homePage = loginPage.login("barney", "barney", RefappHomePage.class);
        assertTrue("The login must have been successful.", homePage.getHeader().isLoggedIn());
        assertFalse("Must not have logged in as admin.", homePage.getHeader().isAdmin());
        assertFalse("Must not have logged in as sysadmin.", homePage.getHeader().isSysadmin());
    }

    @Test
    public void testAtUsersPageAfterLoginUserRole() {
        RefappUsersPage usersPage = loginPage.login("barney", "barney", RefappUsersPage.class);

        assertTrue("The login must have been successful.", usersPage.getHeader().isLoggedIn());
        assertFalse("Must not have logged in as admin.", usersPage.getHeader().isAdmin());
        assertFalse("Must not have logged in as sysadmin.", usersPage.getHeader().isSysadmin());

        assertTrue("Page must be redirected to /plugins/servlet/users after log in.", usersPage.isAtUsersPage());
    }

    @Test
    public void testAtUsersPageAfterLoginAdminRole() {
        RefappUsersPage usersPage = loginPage.login("betty", "betty", RefappUsersPage.class);

        assertTrue("The login must have been successful.", usersPage.getHeader().isLoggedIn());
        assertTrue("Must have logged in as admin.", usersPage.getHeader().isAdmin());
        assertFalse("Must have logged in as sysadmin.", usersPage.getHeader().isSysadmin());

        assertTrue("Page must be redirected to /plugins/servlet/users after log in.", usersPage.isAtUsersPage());
    }

    @Test
    public void testAtUsersPageAfterLoginSysAdminRole() {
        RefappUsersPage usersPage = loginPage.login("admin", "admin", RefappUsersPage.class);

        assertTrue("The login must have been successful.", usersPage.getHeader().isLoggedIn());
        assertTrue("Must have logged in as admin.", usersPage.getHeader().isAdmin());
        assertTrue("Must have logged in as sysadmin.", usersPage.getHeader().isSysadmin());

        assertTrue("Page must be redirected to /plugins/servlet/users after log in.", usersPage.isAtUsersPage());
    }
}
