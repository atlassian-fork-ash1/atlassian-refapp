package it.com.atlassian.plugin.refimpl;

import com.atlassian.functest.client.RemoteTestRunner;
import it.com.atlassian.refapp.RefappTestURLs;
import org.junit.runner.RunWith;

/*
 * This test runner runs the test found in the refapp-test-plugin. The reason we are including it in here
 * is so that we only need to spin up one version of refapp, and to avoid relying to heavily on amps magic.
 */
@RunWith(RemoteTestRunner.class)
public class TestRunner {

    @RemoteTestRunner.BaseURL
    public static String base() {
        return RefappTestURLs.BASEURL;
    }

    @RemoteTestRunner.Group(name = "refapp-test")
    public void standardTest() {
    }
}
