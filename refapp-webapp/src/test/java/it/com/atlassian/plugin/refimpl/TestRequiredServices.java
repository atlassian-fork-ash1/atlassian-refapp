package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappPluginIndexPage;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertTrue;

public class TestRequiredServices extends AbstractRefappTestCase {
    @Test
    public void testRequiredServicesAvailable() {
        RefappPluginIndexPage pluginIndexPage = PRODUCT.visit(RefappPluginIndexPage.class);
        Set<String> serviceInterfaces = pluginIndexPage.getServiceInterfaces();

        assertTrue(serviceInterfaces.contains("com.atlassian.plugin.metadata.PluginMetadataManager"));
        assertTrue(serviceInterfaces.contains("com.atlassian.event.api.EventPublisher"));
        assertTrue(serviceInterfaces.contains("com.atlassian.plugin.event.PluginEventManager"));
        assertTrue(serviceInterfaces.contains("com.atlassian.plugin.PluginController"));
        assertTrue(serviceInterfaces.contains("com.atlassian.plugin.PluginAccessor"));

        // Currently used by spring-scanner ProductFilterUtil to detect refapp
        assertTrue(serviceInterfaces.contains("com.atlassian.refapp.api.ConnectionProvider"));

        assertTrue(serviceInterfaces.contains("com.atlassian.scheduler.SchedulerService"));
    }
}
