package it.com.atlassian.plugin.refimpl;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import junit.framework.TestCase;

public class AbstractRefappTestCase {
    protected static final RefappTestedProduct PRODUCT = TestedProductFactory.create(RefappTestedProduct.class);
}
